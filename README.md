# Prueba GOV.CO

_Desarrollo de prueba para aplicar al cargo de desarrollador front-end_


### Pre-requisitos 📋

_Angular CLI_

```
npm i @angular/cli
```


### Instalación 🔧

_Clonar el proyecto_

```
git clone https://gitlab.com/natalyocampo/govco.git
```

_Navegador al directorio del proyecto e instalar las dependencias_

```
cd govco
npm install
```

_Ejecutar_

```
ng serve
```

_Navegar a `http://localhost:4200/`_


## Construido con 🛠️

* [Angular](https://angular.io/docs) - v10.1.2


## Autor ✒️

* [**Leidy Nataly Ocampo Castro**](https://gitlab.com/natalyocampo)



---
⌨️ por [Leidy Ocampo](https://gitlab.com/natalyocampo) 😊