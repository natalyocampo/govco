// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
	firebaseConfig: {
		apiKey: "AIzaSyAtlKfhJTKMtkV_tIZYJAu41xLGqXoulDk",
		authDomain: "govco-a561b.firebaseapp.com",
		databaseURL: "https://govco-a561b.firebaseio.com",
		projectId: "govco-a561b",
		storageBucket: "govco-a561b.appspot.com",
		messagingSenderId: "655428592280",
		appId: "1:655428592280:web:1f3f842544f3c1e3b7a6ed"
	},
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
