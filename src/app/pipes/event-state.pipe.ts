import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'eventState'
})
export class EventStatePipe implements PipeTransform {

  transform(date: string, type?: number): string {
    if (date > formatDate(new Date(), 'yyyy-MM-dd', 'en')) {
      return !type ? 'active' : 'Activo!';
    } else if (date === formatDate(new Date(), 'yyyy-MM-dd', 'en')) {
      return !type ? 'today' : 'Falta un día';
    } else {
      return !type ? 'past' : 'Conoce los resultados';
    }
  }

}
