import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpinionsService {

  constructor(private firestore: AngularFirestore) { }
	
	public createOpinion(opinion:String) {
		return this.firestore.collection('opinions').add({description: opinion, date: new Date()});
	}
}
