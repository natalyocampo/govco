import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class InterestsService {

  constructor(private http: HttpClient) { }
	
	public getInterests(){
		return this.http.get("https://govco-a561b.firebaseio.com/interests.json");
	}
}
