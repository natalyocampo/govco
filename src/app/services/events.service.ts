import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http: HttpClient) { }

	public getEvents(){
		return this.http.get("https://govco-a561b.firebaseio.com/events.json");
	}
}
