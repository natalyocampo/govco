import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }
	
	public getNews(){
		return this.http.get("https://govco-a561b.firebaseio.com/news.json");
	}
}
