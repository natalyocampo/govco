import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  btnCloseAlert() {
    document.getElementById('alert-container').classList.add('d-none');
    document.getElementById('content-home').classList.add('mt-full');
  }

  btnSearchGovCo() {
  }

  changeLang() {
  }

  login() {
  }
}
