import { Component, OnInit } from '@angular/core';
import { EventsService } from '../services/events.service';
import { InterestsService } from '../services/interests.service';
import { NewsService } from '../services/news.service';
import { OpinionsService } from '../services/opinions.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  events:any = [];
  news:any = [];
  interests:any = [];
  opinion:any = '';

  constructor(private eventsService: EventsService, private interestsService: InterestsService, private newsService : NewsService, private opinionService: OpinionsService) { }

  ngOnInit(): void {
	this.eventsService.getEvents().subscribe( data => {
		this.events = data;
	});

	this.interestsService.getInterests().subscribe( data => {
		this.interests = data;
	});

	this.newsService.getNews().subscribe( data => {
		this.news = data;
	});

	// utils.init();
    // utils.countCharacter('textarea-opinion', 255);
  }
	
  btnSearchGovCo() {
  }
  
  sendOpinion() {
	this.opinionService.createOpinion(this.opinion).then( result => {
	  if (result) {
		this.opinion = '';
		document.getElementById('success-alert').classList.remove('d-none');
	  }
	});
  }
}